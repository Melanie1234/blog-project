DROP DATABASE blog;

CREATE DATABASE blog;

USE blog;

DROP TABLE article;


CREATE TABLE article(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    img VARCHAR (1000),
    title VARCHAR (255),
    content VARCHAR (5000),
    date DATETIME,
    author VARCHAR (255),
    pseudo VARCHAR (255),
    category VARCHAR (255)
 
);

 insert into article (img, title, content, `date`, author, pseudo, category) VALUES ('https://static.fnac-static.com/multimedia/Images/FR/NR/4f/78/86/8812623/1507-1/tsp20221014064027/Dans-l-epaieur-de-la-chair.jpg', "Dans l'épaisseur de la chair", "Pas facile d'extraire la substantifique moelle de Dans l'épaisseur de la chair, un ouvrage dense, parfois confus et déroutant, dont la lecture exige un minimum de persévérance. Il ne m'a pas été inutile, après coup, de refeuilleter quelques pages et de prendre du recul pour étayer mon opinion d'ensemble.
S'agit-il d'un livre d'histoire, d'une saga familiale, de la biographie romancée d'un « pied-noir » nommé Manuel Cortès, ou de l'hommage tardif d'un homme à un père très âgé ?
Dans l'épaisseur de la chair est un peu tout cela à la fois. Jean-Marie Blas de Roblès, l'auteur, est lui-même une personnalité riche, au parcours complexe. Il est philosophe, archéologue, historien, avant d'être poète et romancier. Dans une interview récente, il déclare : « mon but est de faire de la littérature, pas de raconter l'histoire de ma famille »…
Ma foi, l'on peut très bien faire de la littérature tout en racontant l'histoire de sa famille, et cet ouvrage en témoigne. Il dresse un large panorama historique de l'Algérie coloniale, depuis la conquête par les Français jusqu'à l'indépendance. Sur ce fond très documenté, se superpose le parcours d'une famille modeste de pieds-noirs d'origine espagnole, venue s'installer à Bel Abbès, une ville créée à partir d'une ancienne antenne des troupes du général Bugeaud. Emerge ensuite la personnalité du dénommé Manuel Cortès. Il est le père du narrateur, ce dernier étant le double de l'auteur.
A dater de l'indépendance et de l'exode des pieds-noirs, le récit prend une tournure résolument autobiographique, même si le personnage central reste Manuel Cortès. Aux documents et aux témoignages sur lesquels il s'appuyait, l'auteur substitue ses propres souvenirs, son vécu personnel d'enfant, de jeune homme, puis d'homme mûr. Ce qui ressort finalement, c'est la prise de conscience par un fils, des blessures endurées par un père tout au long des vicissitudes de sa vie. Encore a-t-il fallu que ce fils se retrouve empêtré dans une situation suffisamment périlleuse pour remonter le fil de sa généalogie, comme dans les fictions où celui qui va mourir repasse en un clin d'oeil le film de sa vie.
A l'instar de nombreux Français d'Algérie de sa génération, Manuel Cortès avait cru en l'avenir radieux promis par la France coloniale. Ses espérances avaient été contrariées par la seconde guerre mondiale, puis balayées par ce qu'on appela les événements d'Algérie, conclus par l'exode des pieds-noirs. J'aime à croire qu'en célébrant les heurts et malheurs de son père, Blas de Roblès a voulu rendre hommage à tous les Français d'Algérie modestes, devenus « les rapatriés », dont nul ne peut dire qu'ils aient été des profiteurs de la colonisation, mais dont il est incontestable qu'ils ont compté parmi les perdants de l'indépendance.
Chez nombre d'entre eux, l'auteur avait déploré l'absence de sensibilité politique, l'aveuglement devant l'absurdité du concept de colonie, un antisémitisme enkysté, et l'incompréhension devant des actes de rébellion qui n'avaient cessé de prendre de l'ampleur dès la fin de la seconde guerre mondiale
Il leur reconnaît une vraie générosité, une propension spontanée à aider son prochain et une tendance méridionale sympathique à l'excès dans la démonstration. Une tendance que l'on retrouve chez lui-même, lorsqu'il ne résiste pas, à côté de références érudites de bon aloi, à l'envie de sortir des mots en pataouète, des anecdotes de café de commerce, des petites blagues éculées et des démonstrations d'enthousiasme « comme là-bas » pour des passions personnelles qu'on a le droit de ne pas partager, comme la pêche, par exemple.
L'écriture, très travaillée, est brillante, flamboyante. Superbe ! Mais Blas de Roblès prend aussi un malin plaisir à égarer son lecteur dans des digressions liées à ses autres ouvrages, ou dans le recours à des cartes de tarot à la symbolique mystérieuse pour titrer les quatre parties de son ouvrage.
J'ai apprécié son respect pour les souffrances des deux communautés qui se sont déchirées sur un sujet qui mit la France au bord de la guerre civile, et qui a laissé des cicatrices douloureuses dans l'épaisseur de la chair de beaucoup de monde. Si j'ai aimé le travail de reconstitution historique et la couleur picaresque du récit, j'ai été moins sensible à la quête de rédemption filiale. Après tout, c'est son père, pas le mien.", "2023-02-07", "Jean-Marie Blas de Roblès", "Hélène", "Roman autobiographique");
/* 2023-02-07 14:36:29 [8 ms] */ 
insert into article (img, title, content, `date`, author, pseudo, category) VALUES ('https://www.babelio.com/couv/CVT_cvt_Glace_807.jpg', "Glacé", "Indridason, Mankell ou Nesbo n'ont pas leur pareil pour envelopper leurs histoires d'une atmosphère frissonnante ; quel bonheur de découvrir un écrivain français du même acabit !
Avec Bernard Minier point de paysages scandinaves mais nos belles Pyrénées dépeintes avec la passion de l'autochtone
Pour captiver l'amateur de polars sur plus de 700 pages, un cadre, fût-il aussi grandiose, ne suffit pas. Il faut de surcroît un enquêteur à forte personnalité voire atypique et bien sûr une intrigue savamment distillée où phases de réflexion et rebondissements alternent comme pluie et soleil à la pointe bretonne.
Le commandant Martin Servaz est un homme entre deux âges, séparé de sa femme. Le comportement bizarre de Margot, leur fille adolescente vivant chez sa mère, présentement le contrarie. Quelque peu désabusé par la médiocrité du monde qui l'entoure, il serait plutôt de la vieille école appréciant les symphonies au romantisme crépusculaire de Gustav Mahler et jurant de temps à autre en latin.
“Glacé” débute alors même que le corps décapité d'un magnifique yearling, appartenant à un homme d'affaires immensément riche, est retrouvé pendu au plus haut point d'un téléphérique. Cet acte pour le moins barbare a été commis à proximité de l'Institut Wargnier basé en périphérie d'un patelin enneigé et abritant un panel de psychopathes européens parmi les plus dangereux.
La perplexité de Servaz et de ses collègues n'en est qu'à ses débuts : les crimes par pendaison bien vite s'enchaînent et frappent des personnalités de ce petit village jusque-là sans histoire.
Paru en 2011, il s'agit du premier roman policier de Bernard Minier.
Au vu du talent de l'auteur, les enquêtes du commandant Servaz pourraient bien au fil du temps devenir aussi prisées que celles de ses confrères islandais, suédois ou norvégien.", '2023-07-02', 'Bernard Minier', "Eliane", "Roman policier");


