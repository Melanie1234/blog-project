<?php

namespace App\Entities;
use DateTime;

class Article
{
	private ?int $id;

	private ?string $img;

	private ?string $title;

	private ?string $content;

	private ?DateTime $date;

	private ?string $author;

	private ?string $pseudo;


	private ?string $category;



	/**
	 * @param int|null $id
	 * @param string|null $img
	 * @param string|null $title
	 * @param string|null $content
	 * @param DateTime|null $date
	 * @param string|null $author
	 * @param string|null $pseudo

	 * @param string|null $category
	 */
	public function __construct(?int $id, ?string $img, ?string $title, ?string $content, ?DateTime $date, ?string $author, ?string $pseudo, ?string $category) {
		$this->id = $id;
		$this->img = $img;
		$this->title = $title;
		$this->content = $content;
		$this->date = $date;
		$this->author = $author;
		$this->pseudo = $pseudo;
		$this->category = $category;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImg(): ?string {
		return $this->img;
	}
	
	/**
	 * @param string|null $img 
	 * @return self
	 */
	public function setImg(?string $img): self {
		$this->img = $img;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param string|null $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param string|null $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPseudo(): ?string {
		return $this->pseudo;
	}
	
	/**
	 * @param string|null $pseudo 
	 * @return self
	 */
	public function setPseudo(?string $pseudo): self {
		$this->author = $pseudo;
		return $this;
	}
	
	
	/**
	 * @return string|null
	 */
	public function getCategory(): ?string {
		return $this->category;
	}
	
	/**
	 * @param string|null $category 
	 * @return self
	 */
	public function setCategory(?int $category): self {
		$this->category = $category;
		return $this;
	}
}