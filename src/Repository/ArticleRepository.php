<?php

namespace App\Repository;

use App\Entities\Article;
use DateTime;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Un Repository, ou DAO (Data Access Object) est une
 * classe dont le but est de concentrer les appels à la base de données,
 * ainsi, le reste de l'application dépendra des repositories et on aura pas 
 * des appels bdd disseminés partout dans l'appli (comme ça, si jamais 
 * la table change, ou le sgbd ou autre, on aura juste le repo à 
 * modifier et pas tous les endroits où on aurait fait ces appels)
 */
class ArticleRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(): array
    {
        /** @var Article[] */
        $articles = [];


        $statement = $this->connection->prepare('SELECT * FROM article');

        $statement->execute();


        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $articles[] = $this->sqlToArticle($item);
        }
        return $articles;

    }

    private function sqlToArticle(array $line): Article
    {
        $date = null;
        if (isset($line['birthdate'])) {
            $date = new DateTime($line['date']);
        }
        //ou bien avec un tertiaire
        //$birthdate = isset($line['birthdate']) ? new DateTime($line['birthdate']):null;
        return new Article($line['id'], $line['img'], $line['title'], $line['content'], $date, $line['author'], $line['pseudo'], $line['category']);
    }

    /**
     * 
     * @param Article $article 
     * @return void 
     */
    public function persist(Article $article)
    {
        $statement = $this->connection->prepare('INSERT INTO article (img, title, content, date, author, pseudo, category) VALUES (:img, :title, :content, :date, :author, :pseudo, :category)');
        $statement->bindValue('img', $article->getImg());
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('content', $article->getContent());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('pseudo', $article->getPseudo());
        $statement->bindValue('category', $article->getCategory());

        $statement->execute();

        $article->setId($this->connection->lastInsertId());

    }

    public function findById(int $id): ?Article
    {
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            return $this->sqlToArticle($result);
        }
        return null;
    }

    public function delete(Article $article)
    {
        $statement = $this->connection->prepare('DELETE  FROM article WHERE id = :id');

        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function update(Article $article)
    {
        $statement = $this->connection->prepare('UPDATE article SET img=:img, title=:title, content=:content, date=:date, author=:author, pseudo=:pseudo, category=:category WHERE id=:id');
        $statement->bindValue('img', $article->getImg());
        $statement->bindValue('title', $article->getTitle());
        $statement->bindValue('content', $article->getContent());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('pseudo', $article->getPseudo());
        $statement->bindValue('id_category', $article->getCategory());
        $statement->bindValue('id', $article->getId());



        $statement->execute();
        return new JsonResponse(null, 204);
    }



}