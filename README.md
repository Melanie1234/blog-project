## Name
Blog

## Description
Ce projet se compose de 2 parties, une front et une back.
Concernant la partie back, les objectifs étaient de créer des articles et de pouvoir les envoyer vers la partie front pour les afficher, de permettre l'apport de nouvelles données via un formulaire et la suppression d'un article par l'utilisateur.rice.
Pour se faire j'ai tout d'abord créer un base de données pour recevoir les articles.
Par la suite, j'ai créé une entité Article, un repository et enfin un controller.
Ces étapes ont été indspensables pour permettre à ma partie back de dialoguer avec la partie front.

## Built With
Ce projet a été réalisé avec Symfony et MySQL.
Le diagramme de classe est consultable dans le dossier conception du projet https://gitlab.com/Melanie1234/blog-project


## Roadmap
Des améliorations sont à prévoir, principalement sur l'aspect optimisation du code mais également sur l'affichage de la date des publications et sur l'option de modification des articles et sur la validation des données. 

## Contact
Pour toutes interrogations ou propositions d'améliorations, vous pouvez me contacter à l'adresse melanie.ferer1@gmail.com